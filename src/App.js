import logo from './logo.svg';
import './App.css';

import React, { Component } from 'react';
// import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

// import Create from './components/create.component';
// import Edit from './components/edit.component';
import Index from './components/index.component';
import Signup from './components/signup.component';
import Nav from './components/Nav';
import Main from './components/main';




class App extends Component {
  HideNav() {
      if (window.location.pathname === '/signup' || window.location.pathname === '/signin') return <Main />;
      return <Nav />;
    }

  render() {
    return (
      <div className="container">
          { this.HideNav() }
      </div>
    );
  }
}




export default App;

