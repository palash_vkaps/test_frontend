import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
// import axios from 'axios';
import Signup from './signup.component';
import Signin from './signin.component';

var nav_style = {
    marginRight : "35px"
  }
  
class Main extends Component {


  render() {
    return (
        <Router>
        <div className="container">
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                  <Link to={'/signup'} className="nav-link" style={nav_style}>SignUp</Link>
                </li>

                <li className="nav-item">
                  <Link to={'/signin'} className="nav-link" style={nav_style}>SignIn</Link>
                </li>
              </ul>
            </div>
          </nav> 
          <br/>
          <Switch>
              <Route path='/signin' component={ Signin } />
              <Route path='/signup' component={ Signup } />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default Main;


