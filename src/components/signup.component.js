import React, { Component } from 'react';
import axios from 'axios';
import Select from "react-dropdown-select";

export default class Signup extends Component {
	
  constructor(props) {
      super(props);
      this.onChangePhone = this.onChangePhone.bind(this);
      this.onChangeUserEmail = this.onChangeUserEmail.bind(this);
      this.onChangeUserPassword = this.onChangeUserPassword.bind(this);
      this.onChangeGender = this.onChangeGender.bind(this);
      this.onChangeCountry = this.onChangeCountry.bind(this);
      this.onChangeStates = this.onChangeStates.bind(this);
      this.onChangeCity = this.onChangeCity.bind(this);
      this.onChangeSkills = this.onChangeSkills.bind(this);
      this.onChangeDob = this.onChangeDob.bind(this);
      this.onChangeFile = this.onChangeFile.bind(this);
      this.onSubmit = this.onSubmit.bind(this);

      this.state = {
          user_email: '',
          phone: '',
          phone_code:'',
          user_pass:'',
          final_country:'',
          final_state:'',
          final_city:'',
          gender:'',
          male_checked:false,
          female_checked:false,
          skill_qa_checked:false,
          skill_bde_checked:false,
          skill_ba_checked:false,
          skill_hr_checked:false,
          country: [],
          states: [],
          cities: [],
          skills:[],
          file:[],
          dob:'',
          selectedFile: null,
      }
      
  }
  onChangePhone(e) {
  	this.setState({
      phone: e.target.value
    });
  }
  onChangeFile(e){
    this.setState({
      selectedFile: e.target.files[0],
    }, function(){
      console.log(this.state.selectedFile);
    })
  }
  onChangeUserEmail(e) {
  	this.setState({
      user_email: e.target.value
    })  
  }
  onChangeUserPassword(e) {
  	this.setState({
      user_pass: e.target.value
    })
  }
  onChangeGender(e) {
    if (e.target.value == "female") {
      this.setState({
        female_checked: true,
        male_checked: false,
        gender:"female"
      })
    }else{
      this.setState({
        male_checked: true,
        female_checked: false,
        gender:"male"
      })
    }
  }

  onSubmit(e) {
    e.preventDefault();
    let final_phone = this.state.phone_code +" "+ this.state.phone;
    const allData = {
      email: this.state.user_email,
      password:this.state.user_pass,
      phone:final_phone,
      country:this.state.final_country,
      city:this.state.final_city,
      state:this.state.final_state,
      skills:this.state.skills,
      gender:this.state.gender,
      image:"ds"
    }
    console.log(allData);

    axios.post(`http://localhost:2149/insertData`, { allData })
      .then(res => {
        console.log(res);
        console.log(res.data);
      })
    }

  onChangeDob(e){
    this.setState({
        dob: e.target.value
      })
  }

  onChangeSkills(e){
    // let skill = e.target.value;
    var index = this.state.skills.indexOf(e.target.value);
    if (index > -1) 
    {
      this.state.skills.splice(index, 1); 
    }
    else
    {
      this.state.skills.push(e.target.value);
    }

    console.log(this.state.skills);
  }

  onChangeCountry(e){
     console.log(e.target.value);
     const This = this;
     let selected_country = e.target.value;
     axios.get('https://raw.githubusercontent.com/dr5hn/countries-states-cities-database/master/states.json')
      .then(function (response) {
        var states = [];
        for(var i = 0; i<response.data.length; i++)
        {
          if(response.data[i].country_id == selected_country)
          states.push(response.data[i]);
        }
        states = states.map((value, index) => {
            return (<option key={index} value={value.id}>{value.name}</option>);
          });
          This.setState({
              states
            })
            
      }).catch(function (error) {
          console.log(error);
      })

      axios.get('https://raw.githubusercontent.com/dr5hn/countries-states-cities-database/master/countries.json')
        .then(response => {
          for(var i = 0; i<response.data.length; i++)
          {
            if(response.data[i].id == selected_country){
              This.setState({
                final_country : response.data[i].name,
                phone_code : response.data[i].phone_code,
              })

            }
          }
        })

   }

   onChangeStates(e){
     console.log(e.target.value);
     const This = this;
     let selected_state = e.target.value;
     axios.get('https://raw.githubusercontent.com/dr5hn/countries-states-cities-database/master/cities.json')
      .then(function (response) {
        var cities = [];
        for(var i = 0; i<response.data.length; i++)
        {
          if(response.data[i].state_id == selected_state)
          cities.push(response.data[i]);
        }
        cities = cities.map((value, index) => {
            return (<option key={index} value={value.id}>{value.name}</option>);
          });
          This.setState({
              cities
            })
          
      }).catch(function (error) {
          console.log(error);
      })

       axios.get('https://raw.githubusercontent.com/dr5hn/countries-states-cities-database/master/states.json')
        .then(response => {
          for(var i = 0; i<response.data.length; i++)
          {
            if(response.data[i].id == selected_state){
              This.setState({
                final_state : response.data[i].name
              })
            }
          }
        })
   }

onChangeCity(e){
  const This = this;
  console.log(e.target.value);
  const selected_city = e.target.value;
  axios.get('https://raw.githubusercontent.com/dr5hn/countries-states-cities-database/master/cities.json')
        .then(response => {
          for(var i = 0; i<response.data.length; i++)
          {
            if(response.data[i].id == selected_city){
              This.setState({
                final_city : response.data[i].name
              })
            }
          }
        })
}

  componentDidMount(){
      const This = this;
      axios.get('https://raw.githubusercontent.com/dr5hn/countries-states-cities-database/master/countries.json')
        .then(response => {
          var country = [];
          for(var i = 0; i<response.data.length; i++)
          {
            country.push(response.data[i]);
          }
          country = country.map((value, index) => {
            return (<option key={index} value={value.id} data-name={value.name} >{value.name}</option>);
          });
          This.setState({
              country
            })
        })
        .catch(function (error) {
          console.log(error);
        })
    }
 


  render() {
      return (
      	<div>
      		
          <div style={{ marginTop: 10 }}>
              <h3>Signup Here:</h3>
              { this.email_exist == 'yes' ? <span style={{color:"red"}}> This email already exist. </span> : ''}
              <form onSubmit={this.onSubmit}>
                  <div className="form-group">
                      <label>Email: </label>
                      <input type="text" 
                        className="form-control"
                        value={this.state.user_email}
                        onChange={this.onChangeUserEmail}
                        />
                       {this.state.emailError != 'valid' ? <span style={{color:"red"}}> {this.state.emailError} </span> : ''}
                  </div>
                  <div className="">
                      <label>Gender: </label>
                      <div className="radio">
                        <label>
                          <input type="radio" 
                          value="male" 
                          checked={ this.state.male_checked } 
                          onChange={this.onChangeGender}/>
                          Male
                        </label>
                      </div>
                      <div className="radio">
                        <label>
                          <input type="radio" 
                          value="female" 
                          checked={ this.state.female_checked } 
                          onChange={this.onChangeGender}/>
                          Female
                        </label>
                      </div>
                       {this.state.emailError != 'valid' ? <span style={{color:"red"}}> {this.state.emailError} </span> : ''}
                  </div>
                  <div className="form-group">
                      <label>Country: </label>
                      <select className="form-control" id="country" onChange={this.onChangeCountry}>
                      {this.state.country}
                      </select>
                  </div>
                  <div className="form-group">
                      <label>State: </label>
                      <select className="form-control" onChange={this.onChangeStates}>
                      {this.state.states}
                      </select>
                  </div>
                  <div className="form-group">
                      <label>Cities: </label>
                      <select className="form-control" onChange={this.onChangeCity}>
                      {this.state.cities}
                      </select>
                  </div>

                  <div className="form-group">
                      <label>Phone: </label>
                      <div className="country-code-name" style={{ position: "relative" }}>
                        <span style={{ position: "absolute",
                          top: "1px",left: "1px",backgroundColor:" #ebebeb", 
                          width: "30px", textAlign: "center",lineHeight: "32px",
                          borderRadius: "5px 0px 0px 5px" }}>{this.state.phone_code}</span>
                        <input type="number" 
                        className="form-control pnum"
                        style={{paddingLeft: "35px"}}
                        value={this.state.phone}
                        onChange={this.onChangePhone}
                        />
                      </div>
                  </div>
                  <div className="form-group">
                      <label>Skills: </label>
                      <br/>
                      <input type="checkbox" name="QA" value="QA" 
                       onChange={ this.onChangeSkills }
                      /> 
                      QA<br/>
                      <input type="checkbox" name="BDE" value="BDE" 
                       onChange={ this.onChangeSkills }
                      /> 
                      BDE<br/>
                      <input type="checkbox" name="BA" value="BA" 
                      onChange={ this.onChangeSkills }
                      /> 
                      BA<br/>
                      <input type="checkbox" name="HR" value="HR" 
                      onChange={ this.onChangeSkills }
                      /> 
                      HR
                  </div>

                  <div className="form-group">
                    <label>Date Of Birth:</label>
                    <input type="date" className="form-control" value={ this.state.dob } onChange={ this.onChangeDob } />
                  </div>

                  <div className="form-group">
                      <label>Password: </label>
                      <input type="password" 
                        className="form-control"
                        value={this.state.user_pass}
                        onChange={this.onChangeUserPassword}
                        />
                  </div>
                  <div className="form-group">
                      <label>Profile Pic: </label>
                      <input type="file" 
                        className="form-control"
                        onChange={this.onChangeFile}
                        />
                        {this.state.passwordError != 'valid' ? <span style={{color:"red"}}> {this.state.passwordError} </span> : ''}
                  </div>
                  <div className="form-group">
                      <input type="submit" value="Signup" className="btn btn-primary"/>
                  </div>
              </form>
          </div>
         </div>
      )
  }
}