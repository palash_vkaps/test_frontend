import React, { Component } from 'react';
import Nav from './Nav';

export default class Create extends Component {
  constructor(props) {
      super(props);
      this.onChangePersonName = this.onChangePersonName.bind(this);
      this.onChangeBusinessName = this.onChangeBusinessName.bind(this);
      this.onChangeGstNumber = this.onChangeGstNumber.bind(this);
      this.onSubmit = this.onSubmit.bind(this);

      this.state = {
          person_name: '',
          business_name: '',
          business_gst_number:''
      }
  }
  onChangePersonName(e) {
    this.setState({
      person_name: e.target.value
    });
  }
  onChangeBusinessName(e) {
  	// this.validateEmail(e.target.value);
    this.setState({
      business_name: e.target.value
    })  
  }
  onChangeGstNumber(e) {
    this.setState({
      business_gst_number: e.target.value
    })
  }

  validateEmail(email){
   const pattern = /[a-zA-Z0-9]+[\.]?([a-zA-Z0-9]+)?[\@][a-z]{3,9}[\.][a-z]{2,5}/g;
   const result = pattern.test(email);
   if(result===true){
     this.setState({
       emailError:false,
       business_name:email
     })
   } else{
     this.setState({
       emailError:true,
       business_name:email
     })
   }
 }

  onSubmit(e) {
    e.preventDefault();
    // console.log(`The values are ${this.state.person_name}, ${this.state.business_name}, and ${this.state.business_gst_number}`)
    
    var data = {
   "person_name": this.state.person_name,
   "business_name": this.state.business_name,
   "business_gst_number": this.state.business_gst_number,
    }
    console.log(data);

    var url_param = "person_name="+this.state.person_name+"&business_name="+this.state.business_name+"&business_gst_number="+this.state.business_gst_number;

    fetch("http://localhost:3003/save?"+url_param, {
	   	method: "GET",
	   	headers: {
	    	Accept: 'application/json'
	    },
	    // body: data
    })
    .then(function(response){ 
     // console.log(response);
     return response.json();   
    })
    .then(function(data){ 
    console.log(data)
    if(data['status']=="success")
    {
      // console.log(data);
      alert('Your data has been submitted !!!')
    }else
    {
      alert('record already found for today')
    }
    });

    this.setState({
      person_name: '',
      business_name: '',
      business_gst_number: ''
    })
  }
 
 

  render() {
      return (
      	<div>
      		
          <div style={{ marginTop: 10 }}>
              <h3>Add New Business</h3>
              <form onSubmit={this.onSubmit}>
                  <div className="form-group">
                      <label>Person Name:  </label>
                      <input 
                        type="text" 
                        className="form-control" 
                        value={this.state.person_name}
                        onChange={this.onChangePersonName}
                        />
                  </div>
                  <div className="form-group">
                      <label>Business Name: </label>
                      <input type="text" 
                        className="form-control"
                        value={this.state.business_name}
                        onChange={this.onChangeBusinessName}
                        />
                       {this.state.emailError ? <span style={{color: "red"}}>Please Enter valid email address</span> : ''}
                  </div>
                  <div className="form-group">
                      <label>GST Number: </label>
                      <input type="text" 
                        className="form-control"
                        value={this.state.business_gst_number}
                        onChange={this.onChangeGstNumber}
                        />
                  </div>
                  <div className="form-group">
                      <input type="submit" value="Register Business" className="btn btn-primary"/>
                  </div>
              </form>
          </div>
         </div>
      )
  }
}

