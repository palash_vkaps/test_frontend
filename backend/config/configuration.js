!(function() {
    'use strict';
    
    var configuration = {
    	site_url: 'http://localhost:2149',
        // server: {
        //           socketOptions: {
        //           socketTimeoutMS: 0,
        //           connectTimeoutMS: 0
        //         }
        //       },
		database: {
            user: 'pguser',
            database: 'mydb',
            password: 'postgres123',
            port: 5432                  //Default port, change it if needed
        }
     };
    module.exports = configuration;
})();
